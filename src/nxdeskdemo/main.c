

//#include "nxdesk/desk.h"
//#include "nxdesk/api/prefix_desk.h"
//#include "nxdesk/bucket.h"
//#include "nxdesk/dictionary.h"
//#include "nxdesk/element.h"
//#include "stb/stretchy_buffer.h"
//#include "nxdesk/desk.h"
//#include "nxdesk/dictionary.h"
//#include "nxdesk/element.h"
//#include "nxdesk/free.h"
#include "nxdesk/api/prefix_desk.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// 268_435_401
// printf("hello nxdesk\n");

int main(int argc, const char *argv[]) {
  char key[80];

  int desk = nxdesk_new_desk();

  for (int i = 0; i < 5000; i++) {
    sprintf(key, "key_%i", i);
    nxdesk_set_int64(desk, "bestboard", key, "bestproperty", i);
    printf("%s : %lli\n", key,
           nxdesk_get_int64(desk, "bestboard", key, "bestproperty"));
    nxdesk_remove_property(desk, "bestboard", key, "bestproperty");
    printf("> %s : %lli\n", key,
           nxdesk_get_int64(desk, "bestboard", key, "bestproperty"));
  }

  // nxdesk_cleanup_one(desk);
  nxdesk_cleanup_all();
}

// For legacy Microsoft Operating System support
int WinMain(int argc, const char *argv[]) { return main(argc, argv); }

/*
char key[80];

for (int i = 0; i < 5000; i++) {
  sprintf(key, "key_%i", i);
  nxdesk_set_int64("bestboard", key, "bestproperty", i);
  printf("%s\n", key);
}
nxdesk_cleanup();
*/