#ifndef NXDESK_DICTIONARY_H
#define NXDESK_DICTIONARY_H

#include "bucket.h"
#include "hash.h"
#include "stb/stretchy_buffer.h"
#include <stdlib.h>

typedef struct NxdeskDictionary {
  NxdeskBucket *buckets;
  unsigned long bucket_count;
} NxdeskDictionary;

NxdeskDictionary nxdesk_makeNxdeskDictionary(size_t bucket_count) {
  NxdeskDictionary self;
  self.bucket_count = bucket_count;
  self.buckets = NULL;

  for (int i = 0; i < self.bucket_count; i++) {
    stb_sb_push(self.buckets, nxdesk_makeNxdeskBucket());
  }

  return self;
}

NxdeskDictionary *nxdesk_newNxdeskDictionary(size_t bucket_count) {
  NxdeskDictionary *self = (NxdeskDictionary *)malloc(sizeof(NxdeskDictionary));
  *self = nxdesk_makeNxdeskDictionary(bucket_count);
  return self;
}

size_t nxdesk_dictionary_has_key(NxdeskDictionary *self, const char *key) {
  unsigned long hash = nxdesk_hash_cstring(key, self->bucket_count);
  return nxdesk_bucket_has_id(&self->buckets[hash], key);
}

NxdeskElement nxdesk_dictionary_get(NxdeskDictionary *self, const char *key) {
  unsigned long hash = nxdesk_hash_cstring(key, self->bucket_count);
  return nxdesk_bucket_get(&self->buckets[hash], key);
}

size_t nxdesk_dictionary_update(NxdeskDictionary *self, NxdeskElement data) {
  unsigned long hash = nxdesk_hash_cstring(data.id, self->bucket_count);
  return nxdesk_bucket_update(&self->buckets[hash], data);
}

void nxdesk_dictionary_remove(NxdeskDictionary *self, const char *key) {
  unsigned long hash = nxdesk_hash_cstring(key, self->bucket_count);
  nxdesk_bucket_remove_all(&self->buckets[hash], key);
}

#endif