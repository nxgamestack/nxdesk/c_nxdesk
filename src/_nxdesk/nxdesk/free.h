#ifndef NXDESK_FREE_H
#define NXDESK_FREE_H

#include "bucket.h"
#include "dictionary.h"
#include "element.h"

void nxdesk_dictionary_free(NxdeskDictionary *self, int free_self);

void nxdesk_element_free(NxdeskElement *self, int free_self) {
  switch (self->kind) {
  case NXDESK_ELEMENT_DICTIONARY:
    nxdesk_dictionary_free((NxdeskDictionary *)self->data, 1);
    break;

  default:
    free(self->data);
    break;
  }

  if (free_self)
    free(self);
}

void nxdesk_bucket_free(NxdeskBucket *self, int free_self) {
  for (int i = 0; i < stb_sb_count(self->cells); i++) {
    nxdesk_element_free(&(self->cells[i]), 0);
  }
  stb_sb_free(self->cells);
  if (free_self)
    free(self);
}

void nxdesk_dictionary_free(NxdeskDictionary *self, int free_self) {
  for (int i = 0; i < self->bucket_count; i++) {
    nxdesk_bucket_free(&(self->buckets[i]), 0);
  }
  stb_sb_free(self->buckets);
  if (free_self)
    free(self);
}

#endif