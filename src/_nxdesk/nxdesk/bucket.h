#ifndef NXDESK_BUCKET_H
#define NXDESK_BUCKET_H

#include "element.h"
#include "stb/stretchy_buffer.h"
#include <stdio.h>

typedef struct NxdeskBucket {
  NxdeskElement *cells;
} NxdeskBucket;

size_t nxdesk_bucket_first_free(NxdeskBucket *self) {
  for (int i = 1; i < stb_sb_count(self->cells); i++) {
    if (self->cells[i].kind == NXDESK_ELEMENT_EMPTY) {
      return i;
    }
  }
  return 0;
}

NxdeskBucket nxdesk_makeNxdeskBucket() {
  NxdeskBucket self;
  self.cells = NULL;
  stb_sb_push(self.cells,
              nxdesk_makeNxdeskElement(NULL, NXDESK_ELEMENT_UNKNOWN, ""));
  return self;
}

size_t nxdesk_bucket_has_id(NxdeskBucket *self, const char *id) {
  for (int i = 1; i < stb_sb_count(self->cells); i++) {
    if (self->cells[i].kind != NXDESK_ELEMENT_EMPTY &&
        strcmp(self->cells[i].id, id) == 0) {
      return i;
    }
  }
  return 0;
}

size_t nxdesk_bucket_add(NxdeskBucket *self, NxdeskElement data) {
  size_t index = nxdesk_bucket_first_free(self);
  if (index) {
    nxdesk_element_set_id(&self->cells[index], data.id);
    nxdesk_element_set_data(&self->cells[index], data.data, 1);
    nxdesk_element_set_kind(&self->cells[index], data.kind);
  } else {
    stb_sb_push(self->cells, data);
    index = stb_sb_count(self->cells) - 1;
  }
  return index;
}

size_t nxdesk_bucket_update(NxdeskBucket *self, NxdeskElement data) {
  size_t index = nxdesk_bucket_has_id(self, data.id);
  if (index) {
    nxdesk_element_set_id(&self->cells[index], data.id);
    nxdesk_element_set_data(&self->cells[index], data.data, 1);
    nxdesk_element_set_kind(&self->cells[index], data.kind);
  } else {
    index = nxdesk_bucket_add(self, data);
  }
  return index;
}

NxdeskElement nxdesk_bucket_get(NxdeskBucket *self, const char *id) {
  size_t index = nxdesk_bucket_has_id(self, id);
  return self->cells[index];
}

size_t nxdesk_bucket_remove_first(NxdeskBucket *self, const char *id) {
  size_t index = nxdesk_bucket_has_id(self, id);
  if (index) {
    self->cells[index].kind = NXDESK_ELEMENT_EMPTY;
  }
  return index;
}

void nxdesk_bucket_remove_all(NxdeskBucket *self, const char *id) {
  for (int i = 1; i < stb_sb_count(self->cells); i++) {
    if (self->cells[i].kind != NXDESK_ELEMENT_EMPTY &&
        strcmp(self->cells[i].id, id) == 0) {
      self->cells[i].kind = NXDESK_ELEMENT_EMPTY;
    }
  }
}

#endif