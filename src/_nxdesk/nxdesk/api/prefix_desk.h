#ifndef NXDESK_API_PREFIX_DESK_H
#define NXDESK_API_PREFIX_DESK_H

#include "../desk.h"
#include "../element.h"
#include "stb/stretchy_buffer.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// static NxdeskDesk _nxdesk_desk;
static NxdeskDesk *_nxdesk_desks = NULL;
// static int _nxdesk_desk_initialized = 0;

// int _nxdesk_initialize() {
//  if (!_nxdesk_desk_initialized) {
//    _nxdesk_desk = nxdesk_makeNxdeskDesk();
//    _nxdesk_desk_initialized = 1;
//  }
//  return 0;
//}

// void nxdesk_cleanup_one(int desk_id) {
//  nxdesk_desk_free(&_nxdesk_desks[desk_id], 0);
//}

void nxdesk_cleanup_all() {
  for (int i = 0; i < stb_sb_count(_nxdesk_desks); i++) {
    nxdesk_desk_free(&_nxdesk_desks[i], 0);
    // nxdesk_cleanup_one(i);
  }
  stb_sb_free(_nxdesk_desks);
}

int nxdesk_new_desk() {
  stb_sb_push(_nxdesk_desks, nxdesk_makeNxdeskDesk());
  return stb_sb_count(_nxdesk_desks) - 1;
}

/*
int _nxdesk_set_property(char *board_name, char *token_name,
                         char *property_name, void *value) {
  _nxdesk_initialize();
  if (nxdesk_desk_has_property(_nxdesk_desk, board_name, token_name,
                               property_name)) {
    NxdeskElement *old = nxdesk_desk_get_property(_nxdesk_desk, board_name,
                                                  token_name, property_name);
    // free(old->data);
    // NxdeskElement *tmp = nxdesk_newNxdeskElement();
    // tmp->data = value;
    // tmp->kind = NXDESK_ELEMENT_UNKNOWN;
    // nxdesk_desk_set_property(_nxdesk_desk, board_name, token_name,
    //                         property_name, tmp);
    // free(tmp);

    old->data = value;
    old->kind = NXDESK_ELEMENT_UNKNOWN;
  }
}
*/

int nxdesk_has_board(int desk_id, char *board_name) {
  //_nxdesk_initialize();
  return nxdesk_desk_has_board(&_nxdesk_desks[desk_id], board_name);
}
int nxdesk_has_token(int desk_id, char *board_name, char *token_name) {
  //_nxdesk_initialize();
  return nxdesk_desk_has_token(&_nxdesk_desks[desk_id], board_name, token_name);
}
int nxdesk_has_property(int desk_id, char *board_name, char *token_name,
                        char *property_name) {
  //_nxdesk_initialize();
  return nxdesk_desk_has_property(&_nxdesk_desks[desk_id], board_name,
                                  token_name, property_name);
}

int nxdesk_touch_board(int desk_id, char *board_name) {
  //_nxdesk_initialize();
  return nxdesk_desk_touch_board(&_nxdesk_desks[desk_id], board_name);
}
int nxdesk_touch_token(int desk_id, char *board_name, char *token_name) {
  //_nxdesk_initialize();
  return nxdesk_desk_touch_token(&_nxdesk_desks[desk_id], board_name,
                                 token_name);
}

int nxdesk_get_kind(int desk_id, char *board_name, char *token_name,
                    char *property_name) {
  return nxdesk_desk_get_property(&_nxdesk_desks[desk_id], board_name,
                                  token_name, property_name)
      .kind;
}

void nxdesk_remove_property(int desk_id, char *board_name, char *token_name,
                            char *property_name) {
  nxdesk_desk_remove_property(&_nxdesk_desks[desk_id], board_name, token_name,
                              property_name);
}

long long nxdesk_get_int64(int desk_id, char *board_name, char *token_name,
                           char *property_name) {
  //_nxdesk_initialize();

  NxdeskElement tmp = nxdesk_desk_get_property(
      &_nxdesk_desks[desk_id], board_name, token_name, property_name);

  if (tmp.data) {
    return *(long long *)tmp.data;
  } else {
    return 0;
  }
}

int nxdesk_set_int64(int desk_id, char *board_name, char *token_name,
                     char *property_name, long long value) {
  //_nxdesk_initialize();

  long long *pay = (long long *)malloc(sizeof(long long));
  *pay = value;

  nxdesk_desk_set_property(
      &_nxdesk_desks[desk_id], board_name, token_name,
      nxdesk_makeNxdeskElement(pay, NXDESK_ELEMENT_I64, property_name));

  return 0;
}

double nxdesk_get_float64(int desk_id, char *board_name, char *token_name,
                          char *property_name) {
  //_nxdesk_initialize();

  NxdeskElement tmp = nxdesk_desk_get_property(
      &_nxdesk_desks[desk_id], board_name, token_name, property_name);

  if (tmp.data) {
    return *(double *)tmp.data;
  } else {
    return 0.0;
  }
}

int nxdesk_set_float64(int desk_id, char *board_name, char *token_name,
                       char *property_name, double value) {
  //_nxdesk_initialize();

  double *pay = (double *)malloc(sizeof(double));
  *pay = value;

  nxdesk_desk_set_property(
      &_nxdesk_desks[desk_id], board_name, token_name,
      nxdesk_makeNxdeskElement(pay, NXDESK_ELEMENT_I64, property_name));

  return 0;
}

char *nxdesk_get_cstr(int desk_id, char *board_name, char *token_name,
                      char *property_name) {
  //_nxdesk_initialize();

  char *back;

  NxdeskElement tmp = nxdesk_desk_get_property(
      &_nxdesk_desks[desk_id], board_name, token_name, property_name);

  if (tmp.data) {

    back = (char *)malloc(sizeof(char) * (strlen((const char *)tmp.data) + 1));

    strcpy(back, (const char *)tmp.data);
  } else {
    back = (char *)malloc(sizeof(char) * 2);
    strcpy(back, "");
  }

  return back;
}

int nxdesk_set_cstr(int desk_id, char *board_name, char *token_name,
                    char *property_name, const char *value) {
  //_nxdesk_initialize();

  char *pay = (char *)malloc(sizeof(char) * (strlen(value) + 1));
  strcpy(pay, value);

  nxdesk_desk_set_property(
      &_nxdesk_desks[desk_id], board_name, token_name,
      nxdesk_makeNxdeskElement(pay, NXDESK_ELEMENT_I64, property_name));

  return 0;
}

#endif