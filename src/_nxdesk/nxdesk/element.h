#ifndef NXEDESK_ELEMENT_H
#define NXEDESK_ELEMENT_H

#include <stdlib.h>
#include <string.h>

typedef enum NxdeskElementKind {
  NXDESK_ELEMENT_NULL,    // unintialized
  NXDESK_ELEMENT_EMPTY,   // ready to receive data
  NXDESK_ELEMENT_UNKNOWN, // kind unknown
  NXDESK_ELEMENT_I8,
  NXDESK_ELEMENT_I16,
  NXDESK_ELEMENT_I32,
  NXDESK_ELEMENT_I64,
  NXDESK_ELEMENT_U8,
  NXDESK_ELEMENT_U16,
  NXDESK_ELEMENT_U32,
  NXDESK_ELEMENT_U64,
  // NXDESK_ELEMENT_F8,
  // NXDESK_ELEMENT_F16,
  NXDESK_ELEMENT_F32,
  NXDESK_ELEMENT_F64,
  NXDESK_ELEMENT_CSTRING,
  NXDESK_ELEMENT_DICTIONARY,
} NxdeskElementKind;

typedef struct NxdeskElement {
  NxdeskElementKind kind;
  void *data;
  char id[48];
} NxdeskElement;

void nxdesk_element_set_id(NxdeskElement *self, const char *id) {
  strncpy(self->id, id, 48);
}
void nxdesk_element_set_data(NxdeskElement *self, void *data, int do_free) {
  if (do_free)
    free(self->data);
  self->data = data;
}
void nxdesk_element_set_kind(NxdeskElement *self, NxdeskElementKind kind) {
  self->kind = kind;
}
void *nxdesk_element_get_data(NxdeskElement *self) { return self->data; }

NxdeskElement nxdesk_makeNxdeskElement(void *data, NxdeskElementKind kind,
                                       const char *id) {
  NxdeskElement self;
  self.data = data;
  self.kind = kind;
  strncpy(self.id, id, 48);
  return self;
}

#endif