#ifndef NXDESK_DESK
#define NXDESK_DESK

#include "dictionary.h"
#include "element.h"
#include "free.h"

typedef struct NxdeskDesk {
  NxdeskDictionary data;
} NxdeskDesk;

void nxdesk_desk_free(NxdeskDesk *self, int free_self) {
  nxdesk_dictionary_free(&(self->data), 0);
  if (free_self)
    free(self);
}

NxdeskDesk nxdesk_makeNxdeskDesk() {
  NxdeskDesk self;
  self.data = nxdesk_makeNxdeskDictionary(0xff);
  return self;
}

NxdeskDictionary *nxdesk_desk_get_board(NxdeskDesk *self,
                                        const char *board_name) {
  return ((NxdeskDictionary *)nxdesk_dictionary_get(&(self->data), board_name)
              .data);
}

NxdeskDictionary *nxdesk_desk_get_token(NxdeskDesk *self,
                                        const char *board_name,
                                        const char *token_name) {
  return (NxdeskDictionary *)(nxdesk_dictionary_get(
                                  nxdesk_desk_get_board(self, board_name),
                                  token_name)
                                  .data);
}

NxdeskElement nxdesk_desk_get_property(NxdeskDesk *self, const char *board_name,
                                       const char *token_name,
                                       const char *property_name) {
  return nxdesk_dictionary_get(
      nxdesk_desk_get_token(self, board_name, token_name), property_name);
}

int nxdesk_desk_has_board(NxdeskDesk *self, const char *board_name) {
  return (nxdesk_dictionary_has_key(&(self->data), board_name) != 0);
}

int nxdesk_desk_has_token(NxdeskDesk *self, const char *board_name,
                          const char *token_name) {
  return (nxdesk_desk_has_board(self, board_name) &&
          nxdesk_dictionary_has_key(nxdesk_desk_get_board(self, board_name),
                                    token_name));
}

int nxdesk_desk_has_property(NxdeskDesk *self, const char *board_name,
                             const char *token_name,
                             const char *property_name) {
  return (
      nxdesk_desk_has_token(self, board_name, token_name) &&
      nxdesk_dictionary_has_key(
          nxdesk_desk_get_token(self, board_name, token_name), property_name));
}

int nxdesk_desk_touch_board(NxdeskDesk *self, const char *board_name) {
  if (!nxdesk_desk_has_board(self, board_name)) {
    nxdesk_dictionary_update(
        &(self->data),
        nxdesk_makeNxdeskElement(nxdesk_newNxdeskDictionary(0xff),
                                 NXDESK_ELEMENT_DICTIONARY, board_name));
  }
  return 0;
}

int nxdesk_desk_touch_token(NxdeskDesk *self, const char *board_name,
                            const char *token_name) {
  if (!nxdesk_desk_has_board(self, board_name)) {
    nxdesk_desk_touch_board(self, board_name);
  }
  if (!nxdesk_desk_has_token(self, board_name, token_name)) {
    nxdesk_dictionary_update(
        nxdesk_desk_get_board(self, board_name),
        nxdesk_makeNxdeskElement(nxdesk_newNxdeskDictionary(0xff),
                                 NXDESK_ELEMENT_DICTIONARY, token_name));
  }
  return 0;
}

int nxdesk_desk_set_property(NxdeskDesk *self, const char *board_name,
                             const char *token_name, NxdeskElement data) {
  if (!nxdesk_desk_has_token(self, board_name, token_name)) {
    nxdesk_desk_touch_token(self, board_name, token_name);
  }
  nxdesk_dictionary_update(nxdesk_desk_get_token(self, board_name, token_name),
                           data);
  return 0;
}

NxdeskElement nxdesk_desk_remove_property(NxdeskDesk *self,
                                          const char *board_name,
                                          const char *token_name,
                                          const char *property_name) {
  if (nxdesk_desk_has_property(self, board_name, token_name, property_name)) {
    nxdesk_dictionary_remove(
        nxdesk_desk_get_token(self, board_name, token_name), property_name);
  }
}

// void nxdesk_

#endif