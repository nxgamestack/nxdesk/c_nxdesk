require("mingw64")

workspace "WS_nxdesk"
    configurations { "Debug", "Release" }
    platforms { "l64", "w64", "larm64"}
    filter { "platforms:l64" }
        system "linux"
        architecture "x64"

    filter { "platforms:larm64" }
        system "linux"
        architecture "ARM"

    filter { "platforms:w64" }
        system "windows"
        architecture "x64"
        toolset ("mingw64")
        --entrypoint "WinMainCRTStartup"

project "nxdesk"
    language "C"
    
    kind "SharedLib"
    targetdir "lib/%{cfg.shortname}/libnxdesk/"

    libdirs { "./trd/lib" }
    includedirs { "./trd/include" }

    --links { "GL", "glfw", "m"}
    

    files { "./src/_nxdesk/**.h", "./src/_nxdesk/**.c"}

    filter "configurations:Debug"
        defines { "DEBUG" }
        symbols "On"
        --warnings "Extra"
        enablewarnings { "all" }

    filter "configurations:Release"
        defines { "NDEBUG" }
        optimize "On"


    filter { "platforms:l64" }

    filter { "platforms:w64" }
    

project "nxdeskdemo"
    language "C"
    kind "WindowedApp"
    targetdir "bin/%{cfg.shortname}/nxdeskdemo/"

    libdirs { "./lib/%{cfg.shortname}/libnxdesk/" }
    includedirs { "./src/_nxdesk/", "./trd/include" }

    links { "nxdesk" }

    files { "./src/nxdeskdemo/**.h", "./src/nxdeskdemo/**.c" }
    --links { "GL", "glfw", "m"}

    filter "configurations:Debug"
        defines { "DEBUG" }
        symbols "On"
        enablewarnings { "all" }
        disablewarnings { "parentheses" }

    filter "configurations:Release"
        defines { "NDEBUG" }
        optimize "On"

    filter { "platforms:l64" }

    filter { "platforms:w64" }
